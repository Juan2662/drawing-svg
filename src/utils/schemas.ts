import * as Yup from "yup";
import validateColor from "validate-color";

const colorScheme = Yup.string().test(
  "test-color",
  "Invalid color",
  (color) => !!color && validateColor(color)
);

export const circleSchema = Yup.object({
  fillColor: colorScheme,
  left: Yup.number().min(0),
  top: Yup.number().min(0),
  radius: Yup.number().min(0),
});

export const rectangleSchema = Yup.object({
  fillColor: colorScheme,
  left: Yup.number().min(0),
  top: Yup.number().min(0),
  width: Yup.number().min(0),
  height: Yup.number().min(0),
});

export const lineSchema = Yup.object({
  color: colorScheme,
  x1: Yup.number().min(0),
  x2: Yup.number().min(0),
  y1: Yup.number().min(0),
  y2: Yup.number().min(0),
  thickness: Yup.number().min(0),
});
