import React from "react";
import {
  CircleShapeForm,
  LineShapeForm,
  RectangleShapeForm,
} from "../../components";

import { useShapeContext } from "../../hooks";
import type { ShapeItemType } from "../../context/ShapeContext";

interface Props {
  item: ShapeItemType;
  index: number;
}

const ShapeItem: React.FC<Props> = ({ item, index }) => {
  const { removeShapeItem, setShapeItem } = useShapeContext();

  const handleDelete = () => removeShapeItem(item);

  const handleUpdateOptions = React.useCallback(
    (nextOptions: ShapeItemType["options"]) => {
      setShapeItem({ ...item, options: nextOptions });
    },
    [item]
  );

  const ShapeComponent =
    item.type === "line"
      ? LineShapeForm
      : item.type === "circle"
      ? CircleShapeForm
      : RectangleShapeForm;

  return (
    <ShapeComponent
      key={index}
      item={item}
      index={index}
      onDelete={handleDelete}
      onUpdateOptions={handleUpdateOptions}
    />
  );
};

export default ShapeItem;
