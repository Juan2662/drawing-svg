import React from "react";

import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";

import { useShapeContext } from "../../hooks";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
}));

const ToolsButtonContainer: React.FC = () => {
  const { setShapeItem: addShapeItem } = useShapeContext();
  const classes = useStyles();

  const handleShapeCircleItem = () => addShapeItem({ type: "circle" });
  const handleShapeLineItem = () => addShapeItem({ type: "line" });
  const handleShapeRectangleItem = () => addShapeItem({ type: "rectangle" });

  return (
    <div className={classes.root}>
      <Button
        variant="contained"
        color="primary"
        onClick={handleShapeCircleItem}
      >
        Add Circle
      </Button>

      <Button
        variant="contained"
        color="primary"
        onClick={handleShapeRectangleItem}
      >
        Add Rectangle
      </Button>

      <Button variant="contained" color="primary" onClick={handleShapeLineItem}>
        Add Line
      </Button>
    </div>
  );
};
export default ToolsButtonContainer;
