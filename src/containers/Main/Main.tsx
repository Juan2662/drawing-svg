import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";

import { DrawingBoard } from "../../components";
import ShapeContext, {
  ShapeItemType,
  ShapeType,
} from "../../context/ShapeContext";

const getDefaultOptionsValues = (type?: ShapeType) => {
  if (type === "circle") {
    return {
      fillColor: "blue",
      left: "50",
      top: "50",
      radius: "25",
    };
  }
  if (type === "line") {
    return {
      color: "green",
      thickness: "2",
      x1: "200",
      y1: "80",
      x2: "300",
      y2: "20",
    };
  }
  if (type === "rectangle") {
    return {
      fillColor: "red",
      left: "100",
      top: "100",
      width: "250",
      height: "100",
    };
  }
};
type ShapesState = Record<ShapeType, ShapeItemType[]>;

const Main: React.FC = () => {
  const [shapes, setShapes] = React.useState<ShapesState>({
    line: [],
    circle: [],
    rectangle: [],
  });
  const [canvasSize, setCanvasSize] = React.useState({
    width: 0,
    height: 0,
  });
  const canvasRef = React.useRef<HTMLDivElement>(null);
  const numShapes = React.useMemo(
    () =>
      Object.keys(shapes).reduce(
        (acc, key) => acc + shapes[key as ShapeType].length,
        0
      ),
    [shapes]
  );

  React.useLayoutEffect(() => {
    // listen and update canvas size, useful for viewBox prop in svg
    const getCanvasSize = () => {
      const rect = canvasRef.current?.getBoundingClientRect();
      const width = rect?.width;
      const height = rect?.height;
      if (width && height) setCanvasSize({ width, height });
    };

    getCanvasSize();

    const unsubscribe = window.addEventListener("resize", getCanvasSize);

    return unsubscribe;
  }, []);

  const setShapeItem = React.useCallback((data: Partial<ShapeItemType>) => {
    const type = data.type;
    const id = data.id;
    const options = data.options;
    if (!type) return;

    setShapes((prevShapes) => {
      const sortShapes = { ...prevShapes };

      // if add new shape, delete and sort shapes key
      if (!id) delete sortShapes[type];

      const nextShapes = !id
        ? { [type]: prevShapes[type], ...sortShapes }
        : sortShapes;

      if (id && type) {
        const shapeItem = nextShapes[type].find((shape) => shape.id === id);
        if (shapeItem && options) {
          shapeItem.options = options;
        }
      } else {
        nextShapes[type].unshift({
          id: Math.random().toString(36), // generate unique id
          type,
          options: !options ? getDefaultOptionsValues(type) : options,
          timestamp: Date.now(),
        } as ShapeItemType);
      }
      return nextShapes;
    });
  }, []);

  const removeShapeItem = React.useCallback(
    ({ id, type }: Partial<ShapeItemType>) => {
      if (!id || !type) return;
      setShapes((prevShapes) => {
        const nextShapes = { ...prevShapes };
        nextShapes[type] = nextShapes[type].filter((item) => item.id !== id);
        return nextShapes;
      });
    },
    []
  );

  return (
    <React.Fragment>
      <CssBaseline />
      <ShapeContext.Provider
        value={{
          shapes,
          canvasSize,
          canvasRef,
          setShapeItem,
          removeShapeItem,
          numShapes,
        }}
      >
        <Container>
          <DrawingBoard />
        </Container>
      </ShapeContext.Provider>
    </React.Fragment>
  );
};
export default Main;
