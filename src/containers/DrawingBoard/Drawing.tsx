import React from "react";
import { DrawingBoard as DrawingBoardComponent } from "../../components";

const DrawingBoard: React.FC = () => {
  return <DrawingBoardComponent />;
};

export default DrawingBoard;
