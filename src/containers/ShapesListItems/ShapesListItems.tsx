import React from "react";
import { ShapesListItems as ShapesListItemsComponent } from "../../components";
import { useShapeContext } from "../../hooks";

const ShapesListItems: React.FC = () => {
  const { shapes, numShapes } = useShapeContext();
  return (
    <>
      <ShapesListItemsComponent shapes={shapes} numShapes={numShapes} />
    </>
  );
};

export default ShapesListItems;
