import React from "react";
import * as Yup from "yup"
import { useContext } from "react";
import ShapeContext, { ShapeItemType, LineOptionsType, CircleOptionsType, RectangleOptionsType } from "../context/ShapeContext";

type Params = {
  schema: Yup.AnyObjectSchema;
  values: RectangleOptionsType | CircleOptionsType | LineOptionsType
  options: ShapeItemType['options']
  onUpdateOptions: (values: ShapeItemType['options']) => void
}

export const useShapeContext = () => {
  const context = useContext(ShapeContext);
  return context;
};

export const ValidShapeSchema = ({schema, values, options, onUpdateOptions}: Params) => {
  React.useEffect(() => {
    try {
      const isValid = schema.isValidSync(values);
      const enableUpdate =
        isValid && JSON.stringify(values) !== JSON.stringify(options);
      if (enableUpdate) {
        onUpdateOptions(values);
      }
    } catch (error) {
      console.error(error);
    }
  }, [values, options]);
}