import React from "react";

export type ShapeType = "line" | "rectangle" | "circle";

export type RectangleOptionsType = {
  fillColor?: string;
  left?: number | string;
  top?: number | string;
  width?: number | string;
  height?: number | string;
};

export type CircleOptionsType = {
  fillColor?: string;
  left?: number | string;
  top?: string | number;
  radius?: number | string;
};

export type LineOptionsType = {
  color?: string;
  thickness?: string | number;
  x1?: string | number;
  x2?: string | number;
  y1?: string | number;
  y2?: string | number;
};

export type ShapeItemType = {
  type: ShapeType;
  id?: string;
  options?: RectangleOptionsType | CircleOptionsType | LineOptionsType;
  timestamp?: number;
};

const ShapeContext = React.createContext({
  shapes: {} as Record<ShapeType, ShapeItemType[]>,
  setShapeItem: (item: ShapeItemType) => {},
  removeShapeItem: (item: ShapeItemType) => {},
  numShapes: 0,
  canvasRef: {} as React.RefObject<HTMLDivElement>,
  canvasSize: {
    width: 0,
    height: 0,
  },
});

export default ShapeContext;
