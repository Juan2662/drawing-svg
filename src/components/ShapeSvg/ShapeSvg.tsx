import React from "react";
import type {
  ShapeItemType,
  LineOptionsType,
  CircleOptionsType,
  RectangleOptionsType,
} from "../../context/ShapeContext";

const ShapeSvg = ({ type, ...rest }: ShapeItemType) => {
  switch (type) {
    case "line": {
      const {
        x1,
        x2,
        y1,
        y2,
        thickness,
        color,
      } = rest.options as LineOptionsType;
      return (
        <line
          x1={x1}
          y1={y1}
          x2={x2}
          y2={y2}
          strokeWidth={thickness}
          stroke={color}
        />
      );
    }
    case "circle": {
      const {
        fillColor,
        left: x,
        top: y,
        radius,
      } = rest.options as CircleOptionsType;
      return <circle fill={fillColor} r={radius} cx={x} cy={y} />;
    }
    case "rectangle": {
      const {
        left: x,
        top: y,
        width,
        height,
        fillColor,
      } = rest.options as RectangleOptionsType;

      return (
        <rect x={x} y={y} width={width} height={height} fill={fillColor} />
      );
    }
  }
  return null;
};

export default ShapeSvg;
