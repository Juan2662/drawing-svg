import React from "react";
import Typography from "@material-ui/core/Typography";

import { ShapeItem } from "../../containers";
import type { ShapeType, ShapeItemType } from "../../context/ShapeContext";
interface ShapesListItemsProps {
  shapes: Record<ShapeType, ShapeItemType[]>;
  numShapes: number;
}

const ShapesListItems: React.FC<ShapesListItemsProps> = ({
  shapes,
  numShapes,
}) => {
  return (
    <>
      <Typography variant="h3">Shapes</Typography>
      {numShapes === 0 ? (
        <Typography variant="body1">
          You haven't added any shapes yet
        </Typography>
      ) : (
        Object.keys(shapes).reduce(
          (childrens, key) =>
            childrens.concat(
              shapes[key as ShapeType].map((item: ShapeItemType, index) => (
                <ShapeItem key={item.id} index={index} item={item} />
              ))
            ),
          [] as JSX.Element[]
        )
      )}
    </>
  );
};

export default ShapesListItems;
