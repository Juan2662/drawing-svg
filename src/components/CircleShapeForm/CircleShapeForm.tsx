import React from "react";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import DeleteIcon from "@material-ui/icons/Delete";

import { circleSchema } from "../../utils/schemas";

import type {
  ShapeItemType,
  CircleOptionsType,
} from "../../context/ShapeContext";
import { ValidShapeSchema } from "../../hooks";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  title: {
    fontSize: 14,
  },
  form: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: 200,
    },
  },
  margin: {
    margin: theme.spacing(1),
  },
}));

interface CircleShapeFormProps {
  item: ShapeItemType;
  index: number;
  onUpdateOptions: (values?: ShapeItemType["options"]) => void;
  onDelete: () => void;
}

const CircleShapeForm: React.FC<CircleShapeFormProps> = ({
  item,
  index,
  onDelete,
  onUpdateOptions,
}) => {
  const classes = useStyles();
  const [values, setValues] = React.useState(item.options as CircleOptionsType);
  const { fillColor, left, top, radius } = values;

  const handleChange = (fieldName: string) => (
    ev: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ) => {
    setValues({ ...values, [fieldName]: ev.target.value });
  };

  ValidShapeSchema({
    schema: circleSchema,
    values,
    options: item.options,
    onUpdateOptions,
  });

  return (
    <Card className={classes.root} variant="outlined">
      <Grid container spacing={3} key={item.id}>
        <Grid item xs={3}>
          <Typography
            className={classes.title}
            color="textSecondary"
            gutterBottom
          >
            Circle {index + 1}
          </Typography>
        </Grid>
        <Grid item xs={7}>
          <Grid container spacing={2}>
            <Grid item xs={5}>
              <TextField
                label="Fill Color"
                variant="outlined"
                onChange={handleChange("fillColor")}
                value={fillColor}
              />
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={5}>
              <TextField
                label="Top"
                variant="outlined"
                value={top}
                onChange={handleChange("top")}
              />
            </Grid>
            <Grid item xs={5}>
              <TextField
                label="Left"
                variant="outlined"
                value={left}
                onChange={handleChange("left")}
              />
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={5}>
              <TextField
                label="Radius"
                variant="outlined"
                value={radius}
                onChange={handleChange("radius")}
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={2}>
          <IconButton aria-label="delete" onClick={onDelete}>
            <DeleteIcon color="error" fontSize="large" />
          </IconButton>
        </Grid>
      </Grid>
    </Card>
  );
};
export default CircleShapeForm;
