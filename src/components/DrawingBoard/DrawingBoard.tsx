import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";

import { ToolsButtonContainer, ShapesListItems } from "../../containers";
import { useShapeContext } from "../../hooks";
import ShapeSvg from "../../components/ShapeSvg";
import type { ShapeType, ShapeItemType } from "../../context/ShapeContext";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    backgroundColor: "#D8D8D8",
    height: 420,
    textAlign: "center",
    justifyContent: "center",
    alignItems: "center",
  },
  control: {
    padding: theme.spacing(2),
  },
  emptyLabel: {
    flexGrow: 1,
    textAlign: "center",
    alignSelf: "center",
    justifyContent: "center",
  },
}));

const DrawingBoard: React.FC = () => {
  const classes = useStyles();
  const {
    shapes,
    numShapes,
    canvasRef,
    canvasSize: { width, height },
  } = useShapeContext();
  const spacing = 2;
  const sortShapes = Object.keys(shapes)
    .reduce(
      (arr, key) => arr.concat(shapes[key as ShapeType]),
      [] as ShapeItemType[]
    )
    .sort((a, b) => (a.timestamp || 0) - (b.timestamp || 0));

  return (
    <>
      <Typography variant="h3">Drawing board</Typography>
      <Grid container className={classes.root} spacing={2}>
        <Grid item xs={12}>
          <Grid container justify="center" spacing={spacing}>
            <Grid item xs={12} sm={12} md={12} lg={6} xl={4}>
              <Paper ref={canvasRef} className={classes.paper}>
                {numShapes === 0 || !canvasRef.current ? (
                  <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    style={{ width: "100%", height: "100%" }}
                  >
                    <Grid item>
                      <Typography variant="body1">
                        Your Drawing is Empty
                      </Typography>
                    </Grid>
                  </Grid>
                ) : (
                  <svg
                    viewBox={`0 0 ${width} ${height}`}
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    {sortShapes.map((shape) => (
                      <ShapeSvg key={shape.id} id={shape.id} {...shape} />
                    ))}
                  </svg>
                )}
              </Paper>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Grid container justify="center" spacing={spacing}>
            <Grid item>
              <ToolsButtonContainer />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <ShapesListItems />
    </>
  );
};

export default DrawingBoard;
