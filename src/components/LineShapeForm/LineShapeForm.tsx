import React from "react";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import DeleteIcon from "@material-ui/icons/Delete";

import { lineSchema } from "../../utils/schemas";
import { ValidShapeSchema } from "../../hooks";

import type {
  ShapeItemType,
  LineOptionsType,
} from "../../context/ShapeContext";

type Props = {
  item: ShapeItemType;
  index: number;
  onDelete: () => void;
  onUpdateOptions: (values?: ShapeItemType["options"]) => void;
};

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  title: {
    fontSize: 14,
  },
  form: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: 200,
    },
  },
  margin: {
    margin: theme.spacing(1),
  },
}));

const LineShapeForm: React.FC<Props> = ({
  index,
  onDelete,
  item,
  onUpdateOptions,
}) => {
  const classes = useStyles();
  const [values, setValues] = React.useState(item.options as LineOptionsType);
  const { color, thickness, x1, x2, y1, y2 } = values;

  const handleChange = (fieldName: string) => (
    ev: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
  ) => {
    setValues({ ...values, [fieldName]: ev.target.value });
  };

  ValidShapeSchema({
    schema: lineSchema,
    values,
    options: item.options,
    onUpdateOptions,
  });

  return (
    <Card className={classes.root} variant="outlined">
      <Grid container spacing={3} key={item.id}>
        <Grid item xs={3}>
          <Typography
            className={classes.title}
            color="textSecondary"
            gutterBottom
          >
            Line {index + 1}
          </Typography>
        </Grid>
        <Grid item xs={7}>
          <Grid container spacing={2}>
            <Grid item xs={5}>
              <TextField
                label="Color"
                variant="outlined"
                value={color}
                onChange={handleChange("color")}
              />
            </Grid>
            <Grid item xs={5}>
              <TextField
                label="Thickness"
                variant="outlined"
                value={thickness}
                onChange={handleChange("thickness")}
              />
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={5}>
              <TextField
                label="X1"
                variant="outlined"
                value={x1}
                onChange={handleChange("x1")}
              />
            </Grid>
            <Grid item xs={5}>
              <TextField
                label="X2"
                variant="outlined"
                value={x2}
                onChange={handleChange("x2")}
              />
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={5}>
              <TextField
                label="Y1"
                variant="outlined"
                value={y1}
                onChange={handleChange("y1")}
              />
            </Grid>
            <Grid item xs={5}>
              <TextField
                label="Y2"
                variant="outlined"
                value={y2}
                onChange={handleChange("y2")}
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={2}>
          <IconButton aria-label="delete" onClick={onDelete}>
            <DeleteIcon color="error" fontSize="large" />
          </IconButton>
        </Grid>
      </Grid>
    </Card>
  );
};
export default LineShapeForm;
