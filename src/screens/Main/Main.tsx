import React from "react";
import { Main as MainContainer } from "../../containers";

const Main: React.FC = () => <MainContainer />;

export default Main;
